/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm;

import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.IDataMaper;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.PropertyWrapper;
import cern.fesa.expert2.AwakeCalibrationFactors.utils.Observer;
import cern.fesa.expert2.comm.JapcCommunication;
import cern.fesa.expert2.comm.JapcCommunicationCreator;
import cern.fesa.expert2.comm.designInfo.CommunicationException;
import cern.fesa.expert2.comm.subscription.SubscriptionInfo;
import cern.fesa.expert2.comm.subscription.UpdateInfo;
import cern.fesa.expert2.comm.datatypes.DataMap;

public class PropertyCommunicator extends ISubscriberAdapter {
    String propertyName;
    String deviceName;
    SubscriptionInfo info;
    JapcCommunication comm;
    IDataMaper dataMaper;
    Observer observer;
    private boolean subscribed = false;
    
    public PropertyCommunicator(String fesaDeviceName, String propertyName, IDataMaper dataMaper, Observer observer)
            throws CommunicationException {
        comm = (JapcCommunication) new JapcCommunicationCreator().getCommunication(fesaDeviceName);
        this.propertyName = propertyName;
        this.dataMaper = dataMaper;
        this.observer = observer;
        this.deviceName = fesaDeviceName;
    }

    public void subscribe(String timingUserName) throws CommunicationException {
        try {
            unsubscribe();
        } catch (Exception e) {
            // We catch the exception
        }
        info = comm.subscribe(timingUserName, propertyName, true, this);
        subscribed = true;
    }

    public void unsubscribe() throws CommunicationException {
        comm.unsubscribe(info);
        subscribed = false;
    }

    public Object getData(String timingUserName) throws CommunicationException {
        return dataMaper.getObject(deviceName, comm.getProperty(timingUserName, propertyName));
    }

    @Override
    public void receiveUpdate(UpdateInfo updateInfo, DataMap data) {
        observer.update(dataMaper.getObject(updateInfo.getDevice(), data));
    }

    public void setData(String timingUserName, PropertyWrapper pw) throws CommunicationException {

        comm.setProperty(timingUserName, propertyName,
                dataMaper.getDataMap(comm.getProperty(timingUserName, propertyName), pw));

    }

    public boolean isSubscribed() {
        return subscribed;
    }

}
