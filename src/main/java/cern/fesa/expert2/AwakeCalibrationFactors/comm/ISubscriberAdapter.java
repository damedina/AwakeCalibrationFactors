/**
 * Copyright (c) 2015 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm;

import cern.fesa.expert2.comm.datatypes.DataMap;
import cern.fesa.expert2.comm.designInfo.CommunicationException;
import cern.fesa.expert2.comm.subscription.ISubscriber;
import cern.fesa.expert2.comm.subscription.SubscriptionInfo;
import cern.fesa.expert2.comm.subscription.UpdateInfo;

public class ISubscriberAdapter implements ISubscriber {

    @Override
    public void receiveUpdate(UpdateInfo info, DataMap data) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void receiveInterruptingUpdate(UpdateInfo info, DataMap data) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void handleException(UpdateInfo info, CommunicationException exception) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void done(UpdateInfo info) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void disconnected(SubscriptionInfo info) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void reconnected(SubscriptionInfo info) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void cancelled(SubscriptionInfo info) {
        // TODO Auto-generated method stub
        
    }

}
