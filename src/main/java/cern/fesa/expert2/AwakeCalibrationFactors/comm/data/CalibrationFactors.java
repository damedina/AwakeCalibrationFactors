/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm.data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class CalibrationFactors {

    public CalibrationFactors(float[] horElectOffset, float[] horElectScale, float[] verElectOffset,
            float[] verElectScale) {
        this.horElectOffset = horElectOffset;
        this.horElectScale = horElectScale;
        this.verElectOffset = verElectOffset;
        this.verElectScale = verElectScale;
    }

    public CalibrationFactors(String pathToFile) throws IOException {
        List<String> lines = Files.readAllLines(Paths.get(pathToFile));
//        for (String str : lines) {
//            System.out.println(str);
//        }
        // TODO Auto-generated catch block
        int bpmsNumber = lines.size() - 1;
        horElectOffset = new float[bpmsNumber];
        horElectScale = new float[bpmsNumber];
        verElectOffset = new float[bpmsNumber];
        verElectScale = new float[bpmsNumber];
        for (int i = 0; i < bpmsNumber; i++) {
            String[] line = lines.get(i + 1).split(",");
            horElectOffset[i] = Float.parseFloat(line[0]);
            horElectScale[i] = Float.parseFloat(line[1]);
            verElectOffset[i] = Float.parseFloat(line[2]);
            verElectScale[i] = Float.parseFloat(line[3]);
        }
    }

    public float[] getHorElectOffset() {
        return horElectOffset;
    }

    public float[] getHorElectScale() {
        return horElectScale;
    }

    public float[] getVerElectOffset() {
        return verElectOffset;
    }

    public float[] getVerElectScale() {
        return verElectScale;
    }

    private float[] horElectOffset;
    private float[] horElectScale;
    private float[] verElectOffset;
    private float[] verElectScale;

    public static void main(String[] args) {
        try {
            new CalibrationFactors("/user/damedina/workspace-java/AwakeCalibrationFactors/AwakeCal.csv");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
