/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.config;

public enum FesaProperty {
    
    CalibrationFactors("CalibrationFactors");

    private final String name;

    private FesaProperty(String s) {
        name = s;
    }

    @Override
    public String toString() {
        return this.name;
    }
}

