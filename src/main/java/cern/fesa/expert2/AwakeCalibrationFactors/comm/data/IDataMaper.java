/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm.data;

import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;
import cern.fesa.expert2.comm.datatypes.DataMap;

public interface IDataMaper {
    PropertyWrapper getObject(String deviceName, DataMap dataMap);

    DataMap getDataMap(DataMap emptyDataMap, PropertyWrapper object);

    FesaProperty getProperty();
}
