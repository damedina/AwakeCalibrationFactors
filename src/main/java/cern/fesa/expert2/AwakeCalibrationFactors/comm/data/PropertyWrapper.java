/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm.data;

import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;

public class PropertyWrapper {
    private FesaProperty property;
    private Object data;

    public PropertyWrapper(FesaProperty property, Object data) {
        this.property = property;
        this.data = data;
    }

    public FesaProperty getProperty() {
        return property;
    }

    public Object getPropertyData() {
        return data;
    }
}
