/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.model;


import cern.fesa.expert2.AwakeCalibrationFactors.comm.FesaAdapter;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.IFesaAdapter;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.CalibrationFactors;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.PropertyWrapper;
import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;
import cern.fesa.expert2.AwakeCalibrationFactors.utils.Observer;

public class Model implements Observer {

    String timingUserName = "";

    IFesaAdapter fesaAdapter;

    public Model() {
        fesaAdapter = //new FesaAdapter(this); 
        new FesaAdapter(this);
    }

    @Override
    public void update(Object object) {
        if (object instanceof PropertyWrapper) {
            PropertyWrapper pw = (PropertyWrapper) object;
            System.out.println("update "+pw.getProperty());
            switch (pw.getProperty()) {
            case CalibrationFactors:
                CalibrationFactors cf = (CalibrationFactors) pw.getPropertyData();
                //view.update(cf);
                break;
            default:
                break;
            }
        }

    }

//    public void addObserver(View view1) {
//        this.view = view1;
//    }

    public void subscribe(FesaProperty property) {
        fesaAdapter.subscribe(property, timingUserName);
    }

    public void unsubscribe(FesaProperty property) {
        fesaAdapter.unsubscribe(property);
    }

    public void getData(FesaProperty property) {
        update(fesaAdapter.getData(property, timingUserName));
    }

    public void setTimingUserName(String timingUserName) {
        fesaAdapter.unsubscribe(FesaProperty.CalibrationFactors);
        this.timingUserName = timingUserName;
        fesaAdapter.subscribe(FesaProperty.CalibrationFactors, this.timingUserName);
    }

    public void setData(String timingUserName2, PropertyWrapper propertyWrapper) {
        fesaAdapter.setData(timingUserName2, propertyWrapper);
    }

}
