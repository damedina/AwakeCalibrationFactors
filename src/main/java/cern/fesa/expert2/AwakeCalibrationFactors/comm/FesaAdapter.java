/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm;

import java.util.HashMap;

import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.DataMaperFactory;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.IDataMaper;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.PropertyWrapper;
import cern.fesa.expert2.AwakeCalibrationFactors.config.Config;
import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;
import cern.fesa.expert2.AwakeCalibrationFactors.model.Model;
import cern.fesa.expert2.comm.designInfo.CommunicationException;

public class FesaAdapter implements IFesaAdapter {
    HashMap<FesaProperty, PropertyCommunicator> map = new HashMap<FesaProperty, PropertyCommunicator>();

    DataMaperFactory dataMaperFactory = new DataMaperFactory();
    public FesaAdapter(Model model) {
        for (FesaProperty property : Config.fesaProperties) {
            IDataMaper dataMaper = dataMaperFactory.createDataMaper(property);
            try {
                map.put(property, new PropertyCommunicator(Config.deviceName, property.toString(),
                        dataMaper, model));
            } catch (CommunicationException e) {
                System.err.println("Could not connect to device " + Config.deviceName);
                System.exit(1);
            }
        }
    }

    @Override
    public void subscribe(FesaProperty property, String timingUserName) {
        try {
            map.get(property).subscribe(timingUserName);
        } catch (CommunicationException e) {
            System.err.println("Could not subscribe for timing " + timingUserName);
            e.printStackTrace();
        }
    }

    @Override
    public void unsubscribe(FesaProperty property) {
        try {
            PropertyCommunicator pc = map.get(property);
            if (pc.isSubscribed()) {
                pc.unsubscribe();
            }
        } catch (CommunicationException e) {
            System.err.println("Could not unsubscribe.");
        }
    }

    @Override
    public PropertyWrapper getData(FesaProperty property, String timingUserName) {
        PropertyWrapper pw = null;
        try {
            pw = (PropertyWrapper) map.get(property).getData(timingUserName);
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
        return pw;
    }

    @Override
    public void setData(String timingUserName, PropertyWrapper pw) {
        System.out.println("We are going to set data for " + timingUserName + " " + pw.getProperty());
        try {
            map.get(pw.getProperty()).setData(timingUserName, pw);
        } catch (CommunicationException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    public boolean isSubscribed(FesaProperty property) {
        return map.get(property).isSubscribed();
    }
    
    

}
