/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors;

import java.io.IOException;

import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.CalibrationFactors;
import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.PropertyWrapper;
import cern.fesa.expert2.AwakeCalibrationFactors.config.Config;
import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;

public class AwakeCalibrationFactors {
    public static void main(String[] args) {
//        if(args.length != 1) {
//            System.err.println("Invalid number of arguments.");
//            System.err.println("Usage: AwakeCalibrationFactors <timingUserName> <pathToCalibrationFactorsCsv>");
//            System.err.println("Example: AwakeCalibrationFactors AWAKE1 /user/damedina/BPMAWK/calibrationFactors.csv");
//            System.exit(1);
//        }
        
        Controller c = new Controller();
        String timingUserName = Config.domain + ".USER." + "AWAKE1";
        String pathToFile = "/user/damedina/workspace-java/AwakeCalibrationFactors/AwakeCal.csv";
        System.out.println(timingUserName);
        System.out.println(pathToFile);
        try {
            CalibrationFactors cf = new CalibrationFactors(pathToFile);
            c.setData(timingUserName, new PropertyWrapper(FesaProperty.CalibrationFactors, cf));
        } catch (IOException e) {
            System.err.println("Could not read "+pathToFile);
        }
        System.out.println("Done");
        
    }
}
