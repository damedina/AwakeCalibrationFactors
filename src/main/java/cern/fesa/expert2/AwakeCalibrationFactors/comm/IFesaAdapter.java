/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm;

import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.PropertyWrapper;
import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;

public interface IFesaAdapter {

    public void subscribe(FesaProperty property, String timingUserName);

    public void unsubscribe(FesaProperty property);

    public PropertyWrapper getData(FesaProperty property, String timingUserName);
    
    public boolean isSubscribed(FesaProperty property);

    void setData(String timingUserName, PropertyWrapper pw);

}
