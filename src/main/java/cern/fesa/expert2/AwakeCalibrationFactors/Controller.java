/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors;


import cern.fesa.expert2.AwakeCalibrationFactors.comm.data.PropertyWrapper;
import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;
import cern.fesa.expert2.AwakeCalibrationFactors.model.Model;

public class Controller {
    private Model model;
    public Controller() {
        model = new Model();
     }
    
    public void setTimingUserName(String timingUserName) {
        System.out.println("setTimingUserName"+timingUserName);
        model.setTimingUserName(timingUserName);
    }
    public void subscribe(FesaProperty property) {
        model.subscribe(property);
    }

    public void getData(FesaProperty property) {
        model.getData(property);
    }

    public void setData(String timingUserName, PropertyWrapper propertyWrapper) {
        model.setData(timingUserName, propertyWrapper);
    }

}
