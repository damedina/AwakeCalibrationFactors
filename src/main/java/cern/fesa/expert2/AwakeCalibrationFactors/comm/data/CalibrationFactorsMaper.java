/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.comm.data;

import org.apache.log4j.Logger;

import cern.fesa.expert2.AwakeCalibrationFactors.config.FesaProperty;
import cern.fesa.expert2.comm.datatypes.DataMap;

public class CalibrationFactorsMaper implements IDataMaper {

    private static Logger logger = Logger.getLogger(CalibrationFactorsMaper.class);

    @Override
    public PropertyWrapper getObject(String deviceName, DataMap dataMap) {
        CalibrationFactors cs = null;
        if (dataMap != null) {
            float[] horElectOffset = dataMap.getValueAsFloatArray("horElectOffset");
            float[] horElectScale = dataMap.getValueAsFloatArray("horElectOffset");
            float[] verElectOffset = dataMap.getValueAsFloatArray("horElectOffset");
            float[] verElectScale = dataMap.getValueAsFloatArray("horElectOffset");
            cs = new CalibrationFactors(horElectOffset, horElectScale, verElectOffset, verElectScale);
        } else {
            logger.error("Got null DataMap in getObject method of CalibrationSettingsMaper");
        }
        return new PropertyWrapper(getProperty(), cs);

    }

    @Override
    public DataMap getDataMap(DataMap emptyDataMap, PropertyWrapper pw) {

        Object object = pw.getPropertyData();
        System.out.println(emptyDataMap);
        if (object instanceof CalibrationFactors) {
            CalibrationFactors cf = (CalibrationFactors) object;
            emptyDataMap.setValueAsFloatArray("horElecOffset", cf.getHorElectOffset());
            emptyDataMap.setValueAsFloatArray("horElecScale", cf.getHorElectScale());
            emptyDataMap.setValueAsFloatArray("verElecOffset", cf.getVerElectOffset());
            emptyDataMap.setValueAsFloatArray("verElecScale", cf.getVerElectScale());
        } else {
            logger.error("Expected CalibrationSettings object. Could not set DataMap.");
        }
        return emptyDataMap;
    }

    @Override
    public FesaProperty getProperty() {
        return FesaProperty.CalibrationFactors;
    }

}
