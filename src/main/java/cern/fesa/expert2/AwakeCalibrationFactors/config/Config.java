/**
 * Copyright (c) 2016 European Organisation for Nuclear Research (CERN), All Rights Reserved.
 */

package cern.fesa.expert2.AwakeCalibrationFactors.config;

import java.util.ArrayList;
import java.util.Collections;

import cern.fesa.expert2.comm.ccdbdescriptors.CCDBDesignInformer;
import cern.fesa.expert2.comm.ccdbdescriptors.DeviceDescriptor;

public final class Config {
    
    public static final FesaProperty[] fesaProperties = new FesaProperty[] { FesaProperty.CalibrationFactors};
    public static final String deviceName = "BPM.AWAKE.TT41";
    public static final String domain = "SPS";
    public static final boolean storeData = true;
    public static final String dataDirectoryPath = "/user/biswop/data/BPMAWAKE";

    private Config() {
    }

    public static ArrayList<String> getCycles() {

        String className;
        String version;
        DeviceDescriptor devDescr;
        ArrayList<String> cycles = new ArrayList<String>();
        try {
            devDescr = new DeviceDescriptor(deviceName);
            className = devDescr.getClassName();
            version = devDescr.getClassVersion();
            CCDBDesignInformer conf = new CCDBDesignInformer(className, version);
            for (String c : conf.getUsers(deviceName)) {
                cycles.add(String.format("%s.USER.%s", domain, c));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        Collections.sort(cycles);
        return cycles;

    }
}
